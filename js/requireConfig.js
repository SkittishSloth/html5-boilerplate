require.config({
    baseUrl: "js",
    
    paths: {
        /* 3rd Party Stuff */
        "jquery": "lib/jquery-2.0.2.min",
        "knockout": "lib/knockout-2.2.1",
        "jqueryUI": "//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min",
        
        /* Knockout Plugins */
        "pager": "lib/pagerjs/pager.min",
    },
    
    map: {
        "*": {
            "css": "lib/require-css/css"
        }
    },
    
    shim: {
        "jqueryUI": {
            deps: ["jquery", "css!//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui"],
            exports: "jqueryUI"
        },
        
        "pager": {
            deps: ["jquery", "knockout", "css!lib/pagerjs/pager"],
            exports: "pager"
        }
    }
});

require(["appMain"]);
